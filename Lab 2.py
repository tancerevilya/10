from math import cosh, log

x = float(input('Enter x:'))
a = float(input('Enter a:'))
h = float(input('Enter h:'))

if (h == 1):
    g = -(10*(18*a**2+11*a*x-24*x**2)/(-a**2+a*x+6*x**2))
    print('G={}'. format(g))
elif (h == 2):    
    j = (21 * a ** 2 - 34 * a * x + 9 * x ** 2)
    if (j > -710.000001) and (j < 710.000001):
        f = cosh(j)
        print('F={}'. format(f))
    else:
        print('Значение cosh не может быть вычислено')
elif (h == 3):
    i = (3 * a ** 2 - 25 * a * x + 8 * x ** 2 + 1) 
    if i < 0:
        print('Логорифм от отрицательного числа расчитать не возможно')
    else:
        y = log(i) / log(10)
        print('Y={}'.format(y))
else: print('Введенное значение не соответствует требуемому диапазону')
