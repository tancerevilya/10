from math import cosh, log

x1 = float(input('Введите x1:'))
x2 = float(input('Введите x2:'))
a = float(input('Введите a:'))
s = int(input('Введите количество шагов:'))
i = (x2-x1) / s
x = x1

while x < x2:
    g = -(10*(18*a**2+11*a*x-24*x**2)/(-a**2+a*x+6*x**2))
    print ('G={}'. format(g))
    
    h = (3 * a ** 2 - 25 * a * x + 8 * x ** 2 + 1) 
    if h < 0:
        print('Логорифм от отрицательного числа расчитать не возможно')
    else:
        y = log(h) / log(10)
        print('Y={}'.format(y))

    j = (21 * a ** 2 - 34 * a * x + 9 * x ** 2)
    if (j > -710.000001) and (j < 710.000001):
        f = cosh(j)
        print('F={}'. format(f))
    else:
        print('Значение cosh не может быть вычислено')

    x = x + i
    ex=str(input('Если хотите закончить, введите exit: '))
    if ex=='exit':
        break
