from math import cosh, log

str_x1 = str(input('Введите x1: '))
str_x2 = str(input('Введите x2: '))
str_a = str(input('Введите a: '))
str_s = str(input('Введите количество шагов: '))

x1 = float(str_x1)
x2 = float(str_x2)
a = float(str_a)
s = int(str_s)
i = (x2-x1) / s
x = x1

mass_g = []
mass_y = []
mass_f = []
mass_x = []

while x < x2:
    g = -(10*(18*a**2+11*a*x-24*x**2)/(-a**2+a*x+6*x**2))
    mass_g.append(g)
    h = (3 * a ** 2 - 25 * a * x + 8 * x ** 2 + 1) 
    if h < 0:
        print('Логарифм от отрицательного числа расчитать не возможно')
        mass_y.append('xxx')
    else:
        y = log(h) / log(10)
        mass_y.append(y)
    j = (21 * a ** 2 - 34 * a * x + 9 * x ** 2)
    if (j > -710.000001) and (j < 710.000001):
        f = cosh(j)
        mass_f.append(f)
    else:
        print('Значение cosh не может быть вычислено')
        mass_f.append('xxx')
    mass_x.append(x)

    x = x + i

print('')

print('{:<22} {:<20} {:<20} {:<20}'.format('Аргумент', 'G', 'Y', 'F'))
o=0
while o<len(mass_x):
    massiv = ['{:<20} {:<20} {:<20} {:<20}'.format(mass_x[o], mass_g[o], mass_y[o], mass_f[o])]
    print(massiv)
    o=o+1
  
print('')

rez = ['G', 'Y', 'F'] #создаем массив структур
slovar = {
    'G': g, #key: value
    'Y': y,
    'F': f,
}
print(slovar)
