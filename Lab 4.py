from math import cosh, log

x1 = float(input('Введите x1: '))
x2 = float(input('Введите x2: '))
a = float(input('Введите a: '))
s = int(input('Введите количество шагов: '))
i = (x2-x1) / s
x = x1

mass_g = []
mass_y = []
mass_f = []
mass_x = []
while x < x2:
    g = -(10*(18*a**2+11*a*x-24*x**2)/(-a**2+a*x+6*x**2))
    mass_g.append(g) #добавление переменной 
    h = (3 * a ** 2 - 25 * a * x + 8 * x ** 2 + 1) 
    if h < 0:
        print('Логорифм от отрицательного числа расчитать не возможно')
        mass_y.append('xxx') #выводится при наличии пустого значения
    else:
        y = log(h) / log(10)
        mass_y.append(y)
    j = (21 * a ** 2 - 34 * a * x + 9 * x ** 2)
    if (j > -710.000001) and (j < 710.000001):
        f = cosh(j)
        mass_f.append(f)
    else:
        print('Значение cosh не может быть вычислено')
        mass_f.append('xxx')
    mass_x.append(x) #выводится при наличии пустого значения

    x = x + i
    
    mass = [g, y, f]
    print(mass)

    ex=str(input('Если хотите закончить, введите exit: '))
    if ex=='exit':
        break

print ("Максимальное значение массива =", max(mass))
print ("Минимальное значение массива =", min(mass))

print('{:<20} {:<20} {:<20} {:<20}'.format('Аргумент', 'G', 'Y', 'F')) #создание таблицы с массивами
o=0
while o<len(mass_x): #сколько элементов в конструкции (len)
    print('{:<20} {:<20} {:<20} {:<20}'.format(mass_x[o], mass_g[o], mass_y[o], mass_f[o]))
    o=o+1
